<?php
/**************************************************************
 * 
 * @Company Name: BlueThink IT Consulting
 * @Author: Pramod Gupta
 * @Date: 29 Oct 2013
 * @Description: all the action which is used in interaction 
 * with billdesk payment gateway
 * 
 ***************************************************************/
 
class BT_Billdesk_IndexController extends Mage_Core_Controller_Front_Action
{
	
	 public function redirectAction()
    {
		$session = Mage::getSingleton('checkout/session');
		$session->setBilldeskStandardQuoteId($session->getQuoteId());
        $this->getResponse()->setBody($this->getLayout()->createBlock('billdesk/standard_redirect')->toHtml());
        $session->unsQuoteId();
        $session->unsRedirectUrl();
    }
    
    public function processAction(){
	
        try {
			
            $response = $this->getRequest()->getPost();
            
           $response['msg'] = "123456|100000002|NA|620|NA|NA|NA|INR|NA|R|345|NA|NA|F|300|pramod@gmail.com|8860772957|100000007|NA|NA|NA|http://127.0.0.1/magento/billdesk/index/process|-236256844"; 
             
            //$response['msg'] = "TRENDSUTRA|100000013 |MEPG2860576221|201211286600817|3765.00|EPG|00006045|NA|INR|3EMI|NA|NA|NA|28-11-2012 21:22:55|0300|NA|356660|jyo.aquarius@gmail.com|9820346435|100390053|NA|NA|NA|NA|Transaction Successful|0lcLJmFeCSQe";
            
            
            
			$responseMsg = explode('|', $response['msg']);

			$stringWithoutCheckSum = implode("|",$responseMsg2);	
			$model = Mage::getModel('billdesk/standard');
			$checksumKey = $model->getChecksumKey();
		    
		    //$checksumKey = '0lcLJmFeCSQe' ;
			$stringWithCheckSumValue = $stringWithoutCheckSum."|".$checksumKey;	
			$newcheckSumValue = crc32($stringWithCheckSumValue);
			/* when checksum value is not match */
			
			/*if($responseCheckSumValue!=$newcheckSumValue){
				//$this->_redirect('billdesk/index/dicline');
				$this->_redirect('billdesk/index/cancel');
			}
			
			if($responseMsg[1]=='NA'){
			//$this->_redirect('billdesk/index/dicline');
			$this->_redirect('billdesk/index/cancel');
			}*/
			
			/* End checksum value is not match */		
			
			$status = $responseMsg[14];
			
			 	
			/* start Update Response billdesk 19 July */
			
			$orderId = $responseMsg[1]; 	
			
				
			/* End Update Response billdesk */
				
			Mage::log("IPN response: ".$responseMsg, null, "billdesk.log"); 
			
			
			$model = Mage::getModel('billdesk/billdesk')->load($orderId,'order_id');
    		$model->setResponse($stringWithCheckSumValue)
			      ->setUpdateTime(now())
			      ->save();  
		      
			Mage::getModel('billdesk/ipn')->processIpnRequest($responseMsg);      
			
				
			if($status == "0300"){
				$this->_redirect('checkout/onepage/success?method=online');
			}else if($status=="0399"){
				$this->_redirect('billdesk/index/cancel');
			}else{
				$this->_redirect('billdesk/index/pending');
			}
			
        } catch (Exception $e) {
            Mage::logException($e);
        }
	}
    
    
    
   
}
